import requests


class TestClass(object):
    url = "https://the-internet.herokuapp.com/context_menu"
    a = "Right-click in the box below to see one called 'the-internet'"
    b = "alibaba"

    def get_page(self):
        r = requests.get(self.url)
        return r.text

    def test_1(self):
        assert self.a in self.get_page(), "string %s is not in the response page" % self.a

    def test_2(self):
        assert self.b in self.get_page(), "string %s is not in the response page" % self.b










